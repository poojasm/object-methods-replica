function pairs(obj) {
  try{
    const keyValueArr = [];

    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        keyValueArr.push([key, obj[key]]);
      }
    }
    return keyValueArr;
  }

  catch(err){
    console.error(err);
  }
}

export default pairs;
