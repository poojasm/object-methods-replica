function invert(obj) {
  try{
    const invertObj = {};

    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        invertObj[obj[key]] = key;
      }
    }
    return invertObj;
  }
  catch(err){
    console.error(err)
  }
}

export default invert;
