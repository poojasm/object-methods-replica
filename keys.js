function keys(obj) {
  try{
    const keysArr = [];

    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        keysArr.push(key);
      }
    }
    return keysArr;
  }
  catch(err){
    console.error(err);
  }
}

export default keys;
