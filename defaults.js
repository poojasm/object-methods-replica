function defaults(obj, defaultProps) {
  try{
    for (let key in defaultProps) {
      if (defaultProps.hasOwnProperty(key) && obj[key] === undefined) {
        obj[key] = defaultProps[key];
      }
    }
    return obj;
  }
  catch(err){
    console.error(err)
  }
  
}

export default defaults;
