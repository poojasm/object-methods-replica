function values(obj) {
  try{
    const valuesArr = [];

    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        valuesArr.push(obj[key]);
      }
    }
    return valuesArr;
  }

  catch(err){
    console.error(err);
  }
}

export default values;
