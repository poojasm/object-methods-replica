function mapObject(obj, cb) {
  try{
    const resObj = {};

    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        resObj[key] = cb(key, obj[key], obj);
      }
    }
    return resObj;
  }

  catch(err){
    console.error(err);
  }
}

export default mapObject;
