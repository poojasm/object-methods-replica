import testObject from "./data.js";
import mapObject from "../mapObject.js";

const callBack = function (key, value, object) {
  return (object[key] = key);
};
const result = mapObject(testObject, callBack);

console.log(result);
